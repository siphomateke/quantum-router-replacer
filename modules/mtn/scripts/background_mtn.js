class MtnModule extends Module {
    constructor() {
        super('MtnModule');
        this.tabTracker = new TabTracker({'urlPatterns' : chrome.runtime.getManifest().content_scripts[0].matches});

        this.events = new Reactor();
        // this.events.registerEvent('onMessage');

        this.tabTracker.events.addEventListener('onTabLoad', (tab) => {
            console.log('Tab loaded: '+tab);
            mtn.sendTabMessage({command : 'getData', dataName:'pie'});
        });

        this.tabTracker.events.addEventListener('onTabUnload', (tab) => {
            console.log('Tab unloaded: '+tab);
        });

        chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
            
        });
    }
    
    /** Gets a single tab from the list of injected tabs */
    getTab() {
        if (this.tabTracker.numTabs == 1) {
            return this.tabTracker.tabs[Object.keys(this.tabTracker.tabs)[0]];
        }
        else {
            return false;
        }
    }

    /**
     * Sends a chrome message to all WhatsApp tabs
     * @param {object} data The message data
     */
    sendTabMessage(data) {
        if (this.tabTracker.numTabs == 1) {
            // Calls the default module sendTabMessage
            super.sendTabMessage(this.getTab().id, data);
        } else if (this.tabTracker.numTabs > 1) {
            console.log('Error: More than one WhatsApp tab found!');
            for (var key in this.tabTracker.tabs) {
                let tab = this.tabTracker.tabs[key];
                super.sendTabMessage(tab.id, data);
            }
        }
        else {
            console.log('Error: No WhatsApp tabs found!');
        }
    }
}

var mtn = new MtnModule();
