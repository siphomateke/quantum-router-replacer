// Wrapping in a function to not leak/modify variables if the script
// was already inserted before.
(function () {
    // Check if content was loaded already
    if (window.hasRun)
        return true; // Will ultimately be passed back to executeScript
    window.hasRun = true;
    
    function bytesConverter(bytes, format) {
        var sizeCodes = {
            'kb' : 1,
            'mb' : 2,
            'gb' : 3,
            'tb' : 4
        };
        return bytes / Math.pow(1024, sizeCodes[format]);
    }
    
    function _recursiveXml2Object($xml) {
        if ($xml.children().length > 0) {
            var _obj = {};
            $xml.children().each(function () {
                var _childObj = ($(this).children().length > 0) ? _recursiveXml2Object($(this)) : $(this).text();
                if ($(this).siblings().length > 0 && $(this).siblings().get(0).tagName == this.tagName) {
                    if (_obj[this.tagName] == null) {
                        _obj[this.tagName] = [];
                    }
                    _obj[this.tagName].push(_childObj);
                } else {
                    _obj[this.tagName] = _childObj;
                }
            });
            return _obj;
        } else {
            return $xml.text();
        }
    }

    function xml2object($xml) {
        var obj = {};
        if ($xml.find('response').length > 0) {
            var _response = _recursiveXml2Object($xml.find('response'));
            obj.type = 'response';
            obj.response = _response;
        } else if ($xml.find('error').length > 0) {
            var _code = $xml.find('code').text();
            var _message = $xml.find('message').text();
            log.warn('MAIN : error code = ' + _code);
            log.warn('MAIN : error msg = ' + _message);
            obj.type = 'error';
            obj.error = {
                code: _code,
                message: _message
            };
        } else if ($xml.find('config').length > 0) {
            var _config = _recursiveXml2Object($xml.find('config'));
            obj.type = 'config';
            obj.config = _config;
        } else {
            obj.type = 'unknown';
        }
        return obj;
    }

    
    function getRouterData(url, callback) {
        $.ajax({
            url: '../' + url,
            type: 'GET',
            error: function (XMLHttpRequest, textStatus) {
                try {
                    console.log('MAIN : getAjaxData(' + url + ') error.');
                    console.log('MAIN : XMLHttpRequest.readyState = ' + XMLHttpRequest.readyState);
                    console.log('MAIN : XMLHttpRequest.status = ' + XMLHttpRequest.status);
                    console.log('MAIN : textStatus ' + textStatus);
                } catch (exception) {
                    console.log(exception);
                }
            },
            success: function (data) {
                console.log(data);
                var xml = $.parseXML(data);
                console.log(xml);
                var $xml = $(xml);
                var ret = xml2object($xml);
                
                var ret = xml2object($(xml));
                if (typeof callback !== 'undefined') {
                    callback(ret);
                }
            }
        });
    }
    
    function onLoad() {
        var $logo = $('.head_dim .logo').detach();
        var $menu = $('#union_main_menu.main_menu ul');
        var $menuItem = $('<li></li>');
        $menuItem.append($logo);
        $menuItem.prependTo($menu);
        
        /*var $tools = $('.head_dim .tools').detach();
        $('.login_wrapper_dim').append($tools);*/
        getRouterData('api/monitoring/traffic-statistics', function (data) {
            // console.log(data);
            if (data.type == 'response') {
                var response = data.response;
                var mb = bytesConverter(parseFloat(response.CurrentDownload), 'mb');
                console.log(mb);
            }
        });
        
        getRouterData('config/global/config.xml', function (data) {
            // console.log(data);
        });
        
        $('.center_box').append('<canvas id="data_usage_chart"></canvas>');
    }
    
    /**
     * Whether the page is loaded or not
     * @returns {boolean} page load state
     */
    function isLoaded() {
        var loginInfo = $(document).find('.login_box_info');
        return loginInfo.length > 0;
    }

    var loadedObserver = false;
    
    /**
     * Initializes the mutation observer for checking if the page is done loading
     */
    function init() {
        if (!isLoaded()) {
            MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

            loadedObserver = new MutationObserver(function (mutations, observer) {
                if (mutations.length > 0) {
                    if (isLoaded()) {
                        onLoad();
                        loadedObserver.disconnect();
                    }
                }
            });

            // define what element should be observed by the observer
            // and what types of mutations trigger the callback
            loadedObserver.observe($(document)[0], {
                subtree: true,
                attributes: true,
                childList: true,
                characterData: true
            });
        } else {
            onLoad();
        }
    }
    
    chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {
            console.log(request);
        }
    );
    
    init();
})();