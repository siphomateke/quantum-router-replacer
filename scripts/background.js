/**
 * Closes the active tab
 */
core.ca.closeActiveTab = () => {core.ca.onActiveTab(core.ca.closeTab)};