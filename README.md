# Quantum Router Replacer

Redesigns Huawei MTN router admin pages for the 21st century.

Has since been succeeded by [quantum-router](https://github.com/siphomateke/quantum-router/). Instead of just injecting CSS to make the router website look better, it actually provides an entirely separate interface to the router with improved functionality.
